using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Enemy Stat")]
public class EnemyStatSO : ScriptableObject
{
    [SerializeField] private int _enemyID;
    public int enemyID => _enemyID;

    [SerializeField] private Sprite _enemySprite;
    public Sprite enemySprite => _enemySprite;

    [SerializeField] private int _enemyMaxHealth;
    public int enemyMaxHealth => _enemyMaxHealth;

    [SerializeField] private int _enemyDamage;
    public int enemyDamage => _enemyDamage;

    [Range(0, 100)]
    [SerializeField] private int _enemyAccuracy;
    public int enemyAccuracy => _enemyAccuracy;

    [SerializeField] private int _pointGiven;
    public int pointGiven => _pointGiven;
}
