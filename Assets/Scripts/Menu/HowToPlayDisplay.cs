using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HowToPlayDisplay : MonoBehaviour
{
    [SerializeField] private Button _showCombat;
    [SerializeField] private Button _showUpgrade;
    [SerializeField] private GameObject _combatScreen;
    [SerializeField] private GameObject _upgradeScreen;
    private int _currentScreen;

    private void Start()
    {
        _currentScreen = 1;
    }

    private void Update()
    {
        ScreenUpdate();
    }

    private void ScreenUpdate()
    {
        switch(_currentScreen)
        {
            case 1:
                _combatScreen.SetActive(true);
                _upgradeScreen.SetActive(false);

                _showCombat.interactable = false;
                _showUpgrade.interactable = true;
                break;
            case 2:
                _upgradeScreen.SetActive(true);
                _combatScreen.SetActive(false);

                _showUpgrade.interactable = false;
                _showCombat.interactable = true;
                break;
        }
    }

    public void ShowCombatScreen()
    {
        _currentScreen = 1;
    }

    public void ShowUpgradeScreen()
    {
        _currentScreen = 2;
    }
}
