using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject _pauseUI;
    private bool _isPaused;

    private void Start()
    {
        DisablePause();
    }

    private void Update()
    {
        TogglePause();

        PauseUIActivation();
    }

    private void TogglePause()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!_isPaused)
            {
                _isPaused = true;
            }
            else if(_isPaused)
            {
                _isPaused = false;
            }
        }
    }

    private void PauseUIActivation()
    {
        if (_isPaused)
        {
            _pauseUI.SetActive(true);
        }
        else if (!_isPaused)
        {
            _pauseUI.SetActive(false);
        }
    }

    public void DisablePause()
    {
        _isPaused = false;
    }

    public void ResetSingletons()
    {
        ChickenParameterSingleton.instance.ClearChickenParameter();
        ChickenStatSingleton.instance.ClearChickenStat();
        PointSingleton.instance.ClearUpgradePoint();

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
