using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    [SerializeField] private AudioSource _attack;
    [SerializeField] private AudioSource _enemySpawn;
    [SerializeField] private AudioSource _enemyDead;
    [SerializeField] private AudioSource _potionSelect;

    public void AttackSoundPlay()
    {
        _attack.Play();
    }

    public void EnemySpawnSoundPlay()
    {
        _enemySpawn.Play();
    }

    public void EnemyDeadSoundPlay()
    {
        _enemyDead.Play();
    }

    public void PotionSelectSoundPlay()
    {
        _potionSelect.Play();
    }
}
