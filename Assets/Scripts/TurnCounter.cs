using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TurnCounter : MonoBehaviour
{
    [SerializeField] private CombatManager _combatManager;
    [SerializeField] private TextMeshProUGUI _currentTurnText;
    public int _currentTurn;

    [SerializeField] private GameObject _goHomeArrow;

    private void Start()
    {
        _currentTurn = 41;
        _goHomeArrow.SetActive(false);
    }

    private void Update()
    {
        _currentTurnText.text = _currentTurn.ToString();
    }

    public void ReduceTurnCountdown()
    {
        _currentTurn -= 1;
    }

    public void ActivateGoHomeArrow()
    {
        _goHomeArrow.SetActive(true);
    }
}
