using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IndicatorBehavior : MonoBehaviour
{
    [SerializeField] private float _fadeOutSpeed;
    [SerializeField] private TextMeshPro _damageNumberText;
    private bool _startFading = false;

    private void Update()
    {
        gameObject.transform.position += new Vector3(0, 1, 0) * Time.deltaTime * _fadeOutSpeed;

        if(_startFading == false)
        {
            StartCoroutine(FadeWithDelay(0.2f));
        }
        else if(_startFading == true)
        {
            _damageNumberText.color -= new Color32(0, 0, 0, 1);
        }
    }

    public void ChangeDamageNumber(int _damageNumber)
    {
        _damageNumberText.text = $"{_damageNumber}";
    }

    private IEnumerator FadeWithDelay(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        _startFading = true;
    }
}
