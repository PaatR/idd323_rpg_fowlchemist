using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemySystem : MonoBehaviour
{
    [SerializeField] private CombatManager _combatManager;

    [Header("Enemy List")]
    [SerializeField] private List<GameObject> _normalEnemy = new List<GameObject>();
    [SerializeField] private List<GameObject> _eliteEnemy = new List<GameObject>();
    private int _eliteEnemyIndex;

    [Header("Spawn Points")]
    [SerializeField] private GameObject _spawnPoint0;
    [SerializeField] private GameObject _spawnPoint1;
    [SerializeField] private GameObject _spawnPoint2;

    [Header("Wave")]
    [SerializeField] private int _wave;
    [SerializeField] private TextMeshProUGUI WaveCountText;

    public GameObject[] _enemyAmount { get; private set; }
    private bool _spawning = false;

    private void Start()
    {
        _wave = 0;
        _eliteEnemyIndex = 0;
    }

    private void Update()
    {
        WaveCountText.text = $"<color=#A8A8A8>Wave </color>" + _wave.ToString();
    }

    public void SpawnEnemy()
    {
        //check if there are no enemy left and hasn't start spawning (so the spawning coroutine won't stack)
        if (_spawning == false)
        {
            StartCoroutine(SpawningThenChangePhase());
        }
    }
    public void EnemyAttack()
    {
        StartCoroutine(AttackThenChangePhase());
    }

    IEnumerator SpawnOneByOne(float _sec)
    {
        _wave += 1;

        _spawning = true;

        //check wave and choose to spawn here
        if(_wave % 5 == 0)
        {
            Debug.Log("Spawning Elite Enemy");
            yield return new WaitForSeconds(_sec);
            EliteEnemySpawning();
        }
        else
        {
            Debug.Log("Spawning Normal Enemy");
            yield return new WaitForSeconds(_sec);
            EnemySpawning(_normalEnemy, Random.Range(0, _normalEnemy.Count), _spawnPoint0);
            yield return new WaitForSeconds(_sec);
            EnemySpawning(_normalEnemy, Random.Range(0, _normalEnemy.Count), _spawnPoint1);
            yield return new WaitForSeconds(_sec);
            EnemySpawning(_normalEnemy, Random.Range(0, _normalEnemy.Count), _spawnPoint2);
        }

        _spawning = false;
    }

    private void EnemySpawning(List<GameObject> _enemyList, int _index, GameObject _spawnPoint)
    {
        Instantiate(_enemyList[_index], _spawnPoint.transform.position, _spawnPoint.transform.rotation);
        _combatManager._soundEffects.EnemySpawnSoundPlay();
    }

    private void EliteEnemySpawning()
    {
        if(_eliteEnemyIndex > (_eliteEnemy.Count - 1))
        {
            _eliteEnemyIndex = 0;
        }

        EnemySpawning(_eliteEnemy, _eliteEnemyIndex, _spawnPoint1);
        _eliteEnemyIndex += 1;
    }

    //Separated actions
    public void CheckEnemyAmount()
    {
        _enemyAmount = GameObject.FindGameObjectsWithTag("Enemy");
    }
    IEnumerator ChangePhase()
    {
        //change phase and reduce turn
        _combatManager._phase = Phase.PlayerTurn;
        _combatManager._turnCounter.ReduceTurnCountdown();

        yield return null;
    }
    IEnumerator EachEnemyAttack(float _delay)
    {
        foreach(GameObject _enemy in _enemyAmount)
        {
            _enemy.transform.position += new Vector3(0, 0.5f, 0);
            _enemy.SendMessage("Attack");
            yield return new WaitForSeconds(_delay);
            _enemy.transform.position -= new Vector3(0, 0.5f, 0);
        }

        Debug.Log($"{_enemyAmount.Length} enemies attacked");
    }
    //Action then change phase
    IEnumerator SpawningThenChangePhase()
    {
        yield return StartCoroutine(SpawnOneByOne(0.75f));

        yield return StartCoroutine(ChangePhase());
    }
    IEnumerator AttackThenChangePhase()
    {
        yield return new WaitForSeconds(1.0f);

        yield return StartCoroutine(EachEnemyAttack(0.4f));

        yield return StartCoroutine(ChangePhase());
    }
}
