using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Phase { Start, PlayerTurn, EnemyTurn, KnockedOut}

public class CombatManager : MonoBehaviour
{
    public EnemySystem _enemySystem;
    public PointSystemInCombat _pointSystemInCombat;
    public DisplayParameter _displayParameter;
    public CombatButtons _combatButtons;
    public TurnCounter _turnCounter;
    public ChickenStatInCombat _chickenStatInCombat;
    public CombatTargetSelection _combatTargetSelection;
    public SoundEffects _soundEffects;

    public Phase _phase;

    public GameObject _attackDamageIndicator;
    public GameObject _attackMissedIndicator;
    public GameObject _healIndicator;

    private void Start()
    {
        _phase = Phase.Start;
        StartPhase();
    }

    private void Update()
    {
        //Always check enemy amount
        _enemySystem.CheckEnemyAmount();

        PlayerTurnPhase();
        CheatWings();
    }

    private void StartPhase()
    {
        //clear remaining upgrade point via singleton void (after use in upgrade scene)
        //already cleared in upgrade
        //PointSingleton.instance.ClearUpgradePoint();

        //read chicken stat singleton and set max health
        _chickenStatInCombat.ReadChickenStatSingleton();
        _chickenStatInCombat.DefineMaxHealth();

        //read chicken parameter singleton
        _displayParameter.ReadChickenParameterSingletonData();

        //read point singleton
        _pointSystemInCombat.ReadPointSingletonData();

        //set stat below 0 back to 0
        _chickenStatInCombat.SetValueToStandardLimit();

        //spawn first lot of enemy and then go to player turn
        _enemySystem.SpawnEnemy();
    }

    private void PlayerTurnPhase()
    {
        //Check if the turn count is above 0. If not, only allow go home.
        if (_turnCounter._currentTurn <= 0)
        {
            _turnCounter.ActivateGoHomeArrow();
            return;
        }

        //Check if the player HP is above 0
        //Dead = Reduce the upgrade point by half, only allow go home
        if (_chickenStatInCombat.knockOut == true)
        {
            _turnCounter.ActivateGoHomeArrow();
            _pointSystemInCombat.ReducePointByHalfOnDeath();
            return;
        }

        //Suvive = Allow button click + Clicking the button will change to Enemy Turn phase
        _combatButtons.CombatButtonInteractableCheck();

        //Allow selecting target
    }

    public void EnemyTurnPhase()
    {
        //If there are enemy, let each of them take an action
        if (_enemySystem._enemyAmount.Length > 0)
        {
            //USE COROUTINE FOR EACH ENEMY + attack the player + then back to player's turn
            _enemySystem.EnemyAttack();
        }

        //If there are no enemy, spawn bunch of them then go to the player turn
        else if (_enemySystem._enemyAmount.Length <= 0)
        {
            _enemySystem.SpawnEnemy();
        }
    }

    public void SavingDataAndChangeScene()
    {
        //occur when the player press GO HOME button

        //show amount of wave reached, amount of point earned, amount of point lost on death

        //save all the singleton data
        _displayParameter.SaveChickenParameter();
        _pointSystemInCombat.SavePoint();

        //clear stat singleton: to get ready for fresh upgrades
        ChickenStatSingleton.instance.ClearChickenStat();

        //change scene to upgrade scene
        SceneManager.LoadScene("StatAndUpgrade");
    }
    public void DisplayDamageIndicator(GameObject _target, int _damage, Vector3 _offset)
    {
        GameObject _indicator = Instantiate(_attackDamageIndicator, _target.transform.position + _offset, _target.transform.rotation);
        _indicator.SendMessage("ChangeDamageNumber", _damage);
        Destroy(_indicator, 1f);
    }

    public void DisplayMissIndicator(GameObject _target, Vector3 _offset)
    {
        GameObject _indicator = Instantiate(_attackMissedIndicator, _target.transform.position + _offset, _target.transform.rotation);
        Destroy(_indicator, 1f);
    }

    public void DisplayHealIndicator(GameObject _target, int _healAmount, Vector3 _offset)
    {
        GameObject _indicator = Instantiate(_healIndicator, _target.transform.position + _offset, _target.transform.rotation);
        _indicator.SendMessage("ChangeDamageNumber", _healAmount);
        Destroy(_indicator, 1f);
    }

    private void CheatWings()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            _chickenStatInCombat.AddWingsAttackDamage(50);
            _chickenStatInCombat.AddWingsAttackAccuracy(100);
        }
    }
}
