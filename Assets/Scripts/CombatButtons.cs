using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatButtons : MonoBehaviour
{
    [SerializeField] private CombatManager _combatManager;

    [SerializeField] private Button _beakAttackButton;
    [SerializeField] private Button _wingsAttackButton;
    [SerializeField] private Button _guardButton;

    public void CombatButtonInteractableCheck()
    {
        if(_combatManager._phase == Phase.PlayerTurn)
        {
            _beakAttackButton.interactable = true;
            _wingsAttackButton.interactable = true;
            _guardButton.interactable = true;
            return;
        }

        _beakAttackButton.interactable = false;
        _wingsAttackButton.interactable = false;
        _guardButton.interactable = false;
    }

    public void BeakAttack()
    {
        _combatManager._chickenStatInCombat.GuardOff();
        int _accuracyRequirement = Random.Range(1, 101);

        //If has target selected
        if(_combatManager._combatTargetSelection._selectedEnemy != null)
        {
            //Debug.Log("<color=yellow>Attack Selected Target</color>");

            if (_combatManager._chickenStatInCombat._beakAttackAccuracy >= _accuracyRequirement)
            {
                //_combatManager._soundEffects.AttackSoundPlay();

                _combatManager._combatTargetSelection._selectedEnemy.SendMessage("TakeDamage", _combatManager._chickenStatInCombat._beakAttackDamage);
                _combatManager.DisplayDamageIndicator(_combatManager._combatTargetSelection._selectedEnemy, _combatManager._chickenStatInCombat._beakAttackDamage, new Vector3(-0.5f, 0.5f, 0));

                Debug.Log($"<color=cyan>Chicken</color> deal {_combatManager._chickenStatInCombat._beakAttackDamage} beak damage. <color=green>Required: { _accuracyRequirement} | Has: {_combatManager._chickenStatInCombat._beakAttackAccuracy}</color>");
            }
            else if (_combatManager._chickenStatInCombat._beakAttackAccuracy < _accuracyRequirement)
            {
                _combatManager.DisplayMissIndicator(_combatManager._combatTargetSelection._selectedEnemy, new Vector3(-0.5f, 0.5f, 0));
                Debug.Log($"<color=cyan>Chicken</color> missed beak attack. <color=orange>Required: { _accuracyRequirement} | Has: {_combatManager._chickenStatInCombat._beakAttackAccuracy}</color>");
            }
        }

        //If not has target selected
        else if(_combatManager._combatTargetSelection._selectedEnemy == null)
        {
            //Debug.Log("<color=yellow>Attack Random Target</color>");

            GameObject _randomTarget = _combatManager._enemySystem._enemyAmount[Random.Range(0, _combatManager._enemySystem._enemyAmount.Length)];

            if (_combatManager._chickenStatInCombat._beakAttackAccuracy >= _accuracyRequirement)
            {
                //_combatManager._soundEffects.AttackSoundPlay();

                _randomTarget.SendMessage("TakeDamage", _combatManager._chickenStatInCombat._beakAttackDamage);
                _combatManager.DisplayDamageIndicator(_randomTarget, _combatManager._chickenStatInCombat._beakAttackDamage, new Vector3(-0.5f, 0.5f, 0));

                Debug.Log($"<color=cyan>Chicken</color> deal {_combatManager._chickenStatInCombat._beakAttackDamage} beak damage. <color=green>Required: { _accuracyRequirement} | Has: {_combatManager._chickenStatInCombat._beakAttackAccuracy}</color>");
            }
            else if (_combatManager._chickenStatInCombat._beakAttackAccuracy < _accuracyRequirement)
            {
                _combatManager.DisplayMissIndicator(_randomTarget, new Vector3(-0.5f, 0.5f, 0));
                Debug.Log($"<color=cyan>Chicken</color> missed beak attack. <color=orange>Required: { _accuracyRequirement} | Has: {_combatManager._chickenStatInCombat._beakAttackAccuracy}</color>");
            }
        }

        StartCoroutine(ChangeToEnemyTurnPhase());
    }

    public void WingsAttack()
    {
        _combatManager._chickenStatInCombat.GuardOff();

        foreach (GameObject _target in _combatManager._enemySystem._enemyAmount)
        {
            int _accuracyRequirement = Random.Range(1, 101);

            if(_combatManager._chickenStatInCombat._wingsAttackAccuracy >= _accuracyRequirement)
            {
                //_combatManager._soundEffects.AttackSoundPlay();

                _target.SendMessage("TakeDamage", _combatManager._chickenStatInCombat._wingsAttackDamage);
                _combatManager.DisplayDamageIndicator(_target, _combatManager._chickenStatInCombat._wingsAttackDamage, new Vector3(-0.5f, 0.5f, 0));

                Debug.Log($"<color=cyan>Chicken</color> deal {_combatManager._chickenStatInCombat._wingsAttackDamage} wings damage. <color=green>Required: { _accuracyRequirement} | Has: {_combatManager._chickenStatInCombat._wingsAttackAccuracy}</color>");
            }
            else if(_combatManager._chickenStatInCombat._wingsAttackAccuracy < _accuracyRequirement)
            {
                _combatManager.DisplayMissIndicator(_target, new Vector3(-0.5f, 0.5f, 0));
                Debug.Log($"<color=cyan>Chicken</color> missed wings attack. <color=orange>Required: { _accuracyRequirement} | Has: {_combatManager._chickenStatInCombat._wingsAttackAccuracy}</color>");
            }
        }

        StartCoroutine(ChangeToEnemyTurnPhase());
    }

    public void Guard()
    {
        GameObject _chicken = GameObject.Find("Chicken");
        _combatManager._chickenStatInCombat.GuardOn();

        _combatManager._chickenStatInCombat.HealReceived(_combatManager._chickenStatInCombat._breastHealing);
        _combatManager.DisplayHealIndicator(_chicken, _combatManager._chickenStatInCombat._breastHealing, new Vector3(1.75f, 0.75f, 0));

        StartCoroutine(ChangeToEnemyTurnPhase());
    }

    IEnumerator ChangeToEnemyTurnPhase()
    {
        _combatManager._phase = Phase.EnemyTurn;
        yield return new WaitForSeconds(0.2f);
        _combatManager.EnemyTurnPhase();
    }

}
