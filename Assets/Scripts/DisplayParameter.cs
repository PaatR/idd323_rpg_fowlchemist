using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayParameter : MonoBehaviour
{
    [Header("CHICKEN PARAMETER")]
    public int _eyes;
    public int _beak;
    public int _wings;
    public int _breast;

    [SerializeField] private GameObject _eyesLevelBar;
    [SerializeField] private GameObject _beakLevelBar;
    [SerializeField] private GameObject _wingsLevelBar;
    [SerializeField] private GameObject _breastLevelBar;

    [SerializeField] Color32 Base;
    [SerializeField] Color32 Bright;

    private void Update()
    {
        //display the level with the dots bar
        DisplayLevelBarDots(_eyesLevelBar, _eyes);
        DisplayLevelBarDots(_beakLevelBar, _beak);
        DisplayLevelBarDots(_wingsLevelBar, _wings);
        DisplayLevelBarDots(_breastLevelBar, _breast);
    }

    public void ReadChickenParameterSingletonData()
    {
        //see if there is the save data or else set it to default
        if (ChickenParameterSingleton.instance.hasLevelData == true)
        {
            SetEyesLevel(ChickenParameterSingleton.instance.LevelEyes);
            SetBeakLevel(ChickenParameterSingleton.instance.LevelBeak);
            SetWingsLevel(ChickenParameterSingleton.instance.LevelWings);
            SetBreastLevel(ChickenParameterSingleton.instance.LevelBreast);
        }
        else if (ChickenParameterSingleton.instance.hasLevelData == false)
        {
            SetEyesLevel(1);
            SetBeakLevel(1);
            SetWingsLevel(1);
            SetBreastLevel(1);
        }
    }

    private void SetEyesLevel(int level)
    {
        _eyes = level;
    }
    private void SetBeakLevel(int level)
    {
        _beak = level;
    }
    private void SetWingsLevel(int level)
    {
        _wings = level;
    }
    private void SetBreastLevel(int level)
    {
        _breast = level;
    }

    private void DisplayLevelBarDots(GameObject _levelBar, int _partLevel)
    {
        Image[] _levelBarDots;
        _levelBarDots = _levelBar.GetComponentsInChildren<Image>();

        switch(_partLevel)
        {
            case 1:
                _levelBarDots[0].color = Bright;
                _levelBarDots[1].color = Base;
                _levelBarDots[2].color = Base;
                _levelBarDots[3].color = Base;
                _levelBarDots[4].color = Base;
                break;
            case 2:
                _levelBarDots[0].color = Bright;
                _levelBarDots[1].color = Bright;
                _levelBarDots[2].color = Base;
                _levelBarDots[3].color = Base;
                _levelBarDots[4].color = Base;
                break;
            case 3:
                _levelBarDots[0].color = Bright;
                _levelBarDots[1].color = Bright;
                _levelBarDots[2].color = Bright;
                _levelBarDots[3].color = Base;
                _levelBarDots[4].color = Base;
                break;
            case 4:
                _levelBarDots[0].color = Bright;
                _levelBarDots[1].color = Bright;
                _levelBarDots[2].color = Bright;
                _levelBarDots[3].color = Bright;
                _levelBarDots[4].color = Base;
                break;
            case 5:
                _levelBarDots[0].color = Bright;
                _levelBarDots[1].color = Bright;
                _levelBarDots[2].color = Bright;
                _levelBarDots[3].color = Bright;
                _levelBarDots[4].color = Bright;
                break;
        }
    }

    public void SaveChickenParameter()
    {
        ChickenParameterSingleton.instance.SaveChickenParameter(_eyes, _beak, _wings, _breast);
    }

    public void AddEyesLevel(int level)
    {
        _eyes += level;
    }
    public void AddBeakLevel(int level)
    {
        _beak += level;
    }
    public void AddWingsLevel(int level)
    {
        _wings += level;
    }
    public void AddBreastLevel(int level)
    {
        _breast += level;
    }
}
