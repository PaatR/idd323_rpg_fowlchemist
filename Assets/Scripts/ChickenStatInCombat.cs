using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChickenStatInCombat : ChickenStatInUpgrade
{
    [SerializeField] private int _currentHealth;
    [SerializeField] private TextMeshPro _healthText;

    [SerializeField] private SpriteRenderer _chickenSprites;
    [SerializeField] private Sprite _alive;
    [SerializeField] private Sprite _dead;

    public bool knockOut { get; private set; }
    public bool _onGuard { get; private set; }
    public int _finalDamage { get; private set; }

    private void Start()
    {
        _chickenSprites.sprite = _alive;
        _onGuard = false;
        knockOut = false;
    }

    private void Update()
    {
        ChickenHealthTextUpdating();

        if(_currentHealth <= 0)
        {
            knockOut = true;
            _chickenSprites.sprite = _dead;
        }
    }

    public void DefineMaxHealth()
    {
        _currentHealth = _maxHealth;
    }

    public void TakeDamage(int _damage)
    {
        if(_onGuard == true)
        {
            _finalDamage = _damage - (_damage * _breastDamageReduction) / 100;
            _currentHealth -= _finalDamage;
            return;
        }

        _finalDamage = _damage;
        _currentHealth -= _finalDamage;
    }

    public void GuardOn()
    {
        _onGuard = true;
    }

    public void GuardOff()
    {
        _onGuard = false;
    }

    public void HealReceived(int _healAmount)
    {
        _currentHealth += _healAmount;
    }

    private void ChickenHealthTextUpdating()
    {
        _healthText.text = _currentHealth.ToString();

        float _healthPercentage = ((float)_currentHealth / (float)_maxHealth) * 100;
        if (_healthPercentage >= 100)
        {
            _healthText.color = new Color32(127, 178, 132, 255);
        }
        else if (_healthPercentage >= 30 && _healthPercentage < 100)
        {
            _healthText.color = new Color32(221, 205, 130, 255);
        }
        else if (_healthPercentage < 30)
        {
            _healthText.color = new Color32(198, 117, 115, 255);
        }
    }
}
