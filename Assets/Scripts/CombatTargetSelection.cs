using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatTargetSelection : MonoBehaviour
{
    public GameObject _selectedEnemy { get; private set; }
    [SerializeField] private GameObject _selectedEnemyMark;
    [SerializeField] private LayerMask _enemyLayer;
    private Vector2 _mousePosition;

    private void Start()
    {
        _selectedEnemy = null;
    }

    private void Update()
    {
        TrackMousePosition();
        IdentifyMarkedEnemy();
        ClearMarkedEnemy();
    }

    private void TrackMousePosition()
    {
        _mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private void IdentifyMarkedEnemy()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if (Physics2D.OverlapPoint(_mousePosition, _enemyLayer))
            {
                //clear the old selected enemy first.
                _selectedEnemy = null;
                
                _selectedEnemy = Physics2D.OverlapPoint(_mousePosition).gameObject;
                SpawnMarkOnClickedEnemy();
            }
        }
    }

    private void ClearMarkedEnemy()
    {
        if(Input.GetMouseButtonDown(1))
        {
            if (Physics2D.OverlapPoint(_mousePosition, _enemyLayer) && _selectedEnemy != null)
            {
                _selectedEnemy = null;
                DestroyOldMarks();
            }
        }
    }

    private void SpawnMarkOnClickedEnemy()
    {
        //destroy all the old spawned marks first.
        DestroyOldMarks();
        Instantiate(_selectedEnemyMark, _selectedEnemy.transform.position + new Vector3(0, 2, 0), _selectedEnemy.transform.rotation);
    }

    public void DestroyOldMarks()
    {
        GameObject[] _unusedMarksList = GameObject.FindGameObjectsWithTag("EnemyMark");
        foreach (GameObject _unusedMarks in _unusedMarksList)
        {
            Destroy(_unusedMarks.gameObject);
        }
    }
}
