using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PointSystemInCombat : PointSystemInUpgrade
{
    private bool _pointReduced;

    private void Start()
    {
        _pointReduced = false;
    }

    public void ReducePointByHalfOnDeath()
    {
        if(_pointReduced == false)
        {
            StartCoroutine(ReducePointByHalfOnDeathOnce());
        }
    }

    IEnumerator ReducePointByHalfOnDeathOnce()
    {
        _pointReduced = true;
        _upgradePoint = _upgradePoint / 2;

        yield return null;
    }
}
