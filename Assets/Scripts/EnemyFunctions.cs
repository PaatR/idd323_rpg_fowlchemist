using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyFunctions : MonoBehaviour
{
    private CombatManager _combatManager;
    [SerializeField] private EnemyStatSO _enemyStat;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private TextMeshPro _healthText;
    private int _enemyCurrentHealth;
    private GameObject _chicken;

    private void Start()
    {
        GameObject _combatScript = GameObject.Find("CombatScripts");
        _combatManager = _combatScript.GetComponent<CombatManager>();

        _chicken = GameObject.Find("Chicken");

        _spriteRenderer.sprite = _enemyStat.enemySprite;
        _enemyCurrentHealth = _enemyStat.enemyMaxHealth;
    }

    private void Update()
    {
        HealthTextUpdating();

        if(_enemyCurrentHealth <= 0)
        {
            _combatManager._pointSystemInCombat.PointIncrease(_enemyStat.pointGiven);
            _combatManager._combatTargetSelection.DestroyOldMarks();

            _combatManager._soundEffects.EnemyDeadSoundPlay();

            Destroy(this.gameObject);
        }
    }

    public void TakeDamage(int _damage)
    {
        _enemyCurrentHealth -= _damage;

        AttackSoundCheck();
    }

    public void Attack()
    {
        //at least 1 percent chance to hit and max 100 percent chance to hit.
        int _accuracyRequirement = Random.Range(1, 101);

        if(_enemyStat.enemyAccuracy >= _accuracyRequirement)
        {
            _combatManager._chickenStatInCombat.TakeDamage(_enemyStat.enemyDamage);
            _combatManager.DisplayDamageIndicator(_chicken, _combatManager._chickenStatInCombat._finalDamage, new Vector3(1.75f, 0.75f, 0));

            _combatManager._soundEffects.AttackSoundPlay();
            
            Debug.Log($"<color=red>Enemy</color> {_enemyStat.enemyID} deal {_combatManager._chickenStatInCombat._finalDamage} damage. <color=green>Required: { _accuracyRequirement} | Has: {_enemyStat.enemyAccuracy}</color>");
        }
        else if(_enemyStat.enemyAccuracy < _accuracyRequirement)
        {
            _combatManager.DisplayMissIndicator(_chicken, new Vector3(1.75f, 0.75f, 0));
            Debug.Log($"<color=red>Enemy</color> {_enemyStat.enemyID} missed. <color=orange>Required: {_accuracyRequirement} | Has: {_enemyStat.enemyAccuracy}</color>");
        }
    }

    private void HealthTextUpdating()
    {
        //Number
        _healthText.text = _enemyCurrentHealth.ToString();

        //Color
        float _healthPercentage = ((float)_enemyCurrentHealth / (float)_enemyStat.enemyMaxHealth) * 100;
        if(_healthPercentage >= 100)
        {
            _healthText.color = new Color32(127, 178, 132, 255);
        }
        else if (_healthPercentage >= 30 && _healthPercentage < 100)
        {
            _healthText.color = new Color32(221, 205, 130, 255);
        }
        else if(_healthPercentage < 30)
        {
            _healthText.color = new Color32(198, 117, 115, 255);
        }
    }

    private void AttackSoundCheck()
    {
        if(_enemyCurrentHealth <= 0)
        {
            return;
        }

        _combatManager._soundEffects.AttackSoundPlay();
    }
}
