using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSingleton : MonoBehaviour
{
    public static PointSingleton instance;

    public int UpgradePoint { get; private set; }
    public bool hasUpgradePointData { get; private set; }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void SaveUpgradePoint(int _upgradePoint)
    {
        UpgradePoint = _upgradePoint;

        hasUpgradePointData = true;
    }

    public void ClearUpgradePoint()
    {
        hasUpgradePointData = false;
    }
}
