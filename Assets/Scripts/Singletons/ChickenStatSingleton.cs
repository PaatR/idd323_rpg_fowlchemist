using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenStatSingleton : MonoBehaviour
{
    public static ChickenStatSingleton instance;

    public int maxHealth { get; private set; }
    public int beakAttackDamage { get; private set; }
    public int beakAttackAccuracy { get; private set; }
    public int wingsAttackDamage { get; private set; }
    public int wingsAttackAccuracy { get; private set; }
    public int breastDamageReduction { get; private set; }
    public int breastHealing { get; private set; }
    public bool hasStatData { get; private set; }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void SaveChickenStat(int _maxHealth, int _beakAttackDamage, int _beakAttackAccuracy, 
        int _wingsAttackDamage, int _wingsAttackAccuracy, int _breastDamageReduction, int _breastHealing)
    {
        maxHealth = _maxHealth;
        beakAttackDamage = _beakAttackDamage;
        beakAttackAccuracy = _beakAttackAccuracy;
        wingsAttackDamage = _wingsAttackDamage;
        wingsAttackAccuracy = _wingsAttackAccuracy;
        breastDamageReduction = _breastDamageReduction;
        breastHealing = _breastHealing;

        hasStatData = true;
    }

    public void ClearChickenStat()
    {
        hasStatData = false;
    }
}
