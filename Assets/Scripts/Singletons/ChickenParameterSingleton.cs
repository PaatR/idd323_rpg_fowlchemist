using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenParameterSingleton : MonoBehaviour
{
    public static ChickenParameterSingleton instance;

    public int LevelEyes { get; private set; }
    public int LevelBeak { get; private set; }
    public int LevelWings { get; private set; }
    public int LevelBreast { get; private set; }
    public bool hasLevelData { get; private set; }

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void SaveChickenParameter(int eyes, int beak, int wings, int breast)
    {
        LevelEyes = eyes;
        LevelBeak = beak;
        LevelWings = wings;
        LevelBreast = breast;

        hasLevelData = true;
    }

    public void ClearChickenParameter()
    {
        hasLevelData = false;
    }
}
