using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AccuracyType { BeakAttackAcc, WingsAttackAcc}

public class PotionFunctions_Eyes : PotionFunctions
{
    [SerializeField] private AccuracyType _accuracyType;
    private int _beakAccuracyAdded;
    private int _wingsAccuracyAdded;

    private void Update()
    {
        PriceAndInteractableUpdate();
        PotionDescriptionAndEffectUpdate();
    }

    public override void OnPotionSelection()
    {
        base.OnPotionSelection();
        AccuracyIncrease();
    }

    private void AccuracyIncrease()
    {
        switch(_accuracyType)
        {
            case AccuracyType.BeakAttackAcc:
                _upgradeManager._chickenStatInUpgrade.AddBeakAttackAccuracy(_beakAccuracyAdded);
                break;
            case AccuracyType.WingsAttackAcc:
                _upgradeManager._chickenStatInUpgrade.AddWingsAttackAccuracy(_wingsAccuracyAdded);
                break;
        }
    }

    private void PotionDescriptionAndEffectUpdate()
    {
        switch(_accuracyType)
        {
            case AccuracyType.BeakAttackAcc:
                _beakAccuracyAdded = 5 * _upgradeManager._displayParameter._eyes;
                _descriptionText.text = $"Beak Attack Accuracy +{_beakAccuracyAdded}";
                break;
            case AccuracyType.WingsAttackAcc:
                _wingsAccuracyAdded = 4 * _upgradeManager._displayParameter._eyes;
                _descriptionText.text = $"Wings Attack Accuracy +{_wingsAccuracyAdded}";
                break;
        }
    }
}
