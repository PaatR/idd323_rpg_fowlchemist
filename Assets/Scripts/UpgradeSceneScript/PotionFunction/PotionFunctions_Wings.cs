using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WingsDebuffType { DebuffWingsAcc, DebuffBreastHealing}

public class PotionFunctions_Wings : PotionFunctions
{
    [SerializeField] private WingsDebuffType _wingsDebuffType;
    private int _wingsDamageAdded;
    private int _wingsAccuracyReduced;
    private int _breastHealingReduced;

    private void Update()
    {
        PriceAndInteractableUpdate();
        PotionDescriptionAndEffectUpdate();
    }

    public override void OnPotionSelection()
    {
        base.OnPotionSelection();
        WingsDamageIncrease();
    }

    private void WingsDamageIncrease()
    {
        switch(_wingsDebuffType)
        {
            case WingsDebuffType.DebuffWingsAcc:
                _upgradeManager._chickenStatInUpgrade.AddWingsAttackDamage(_wingsDamageAdded);
                _upgradeManager._chickenStatInUpgrade.AddWingsAttackAccuracy(-_wingsAccuracyReduced);
                break;
            case WingsDebuffType.DebuffBreastHealing:
                _upgradeManager._chickenStatInUpgrade.AddWingsAttackDamage(_wingsDamageAdded);
                _upgradeManager._chickenStatInUpgrade.AddBreastHealing(-_breastHealingReduced);
                break;
        }
    }

    private void PotionDescriptionAndEffectUpdate()
    {
        switch (_wingsDebuffType)
        {
            case WingsDebuffType.DebuffWingsAcc:
                _wingsDamageAdded = 2 * _upgradeManager._displayParameter._wings;
                _wingsAccuracyReduced = (2 * (6 - _upgradeManager._displayParameter._wings));
                _descriptionText.text = $"Wings Attack Damage +{_wingsDamageAdded}\n" +
                    $"<color=#CB0000>Wings Attack Accuracy -{_wingsAccuracyReduced}</color>";
                break;
            case WingsDebuffType.DebuffBreastHealing:
                _wingsDamageAdded = 2 * _upgradeManager._displayParameter._wings;
                _breastHealingReduced = (1 * (6 - _upgradeManager._displayParameter._wings));
                _descriptionText.text = $"Wings Attack Damage +{_wingsDamageAdded}\n" +
                    $"<color=#CB0000>Breast Healing -{_breastHealingReduced}</color>";
                break;
        }
    }
}
