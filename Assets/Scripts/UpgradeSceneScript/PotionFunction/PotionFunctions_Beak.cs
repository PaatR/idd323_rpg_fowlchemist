using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BeakDebuffType { DebuffBeakAcc, DebuffBreastResistance}

public class PotionFunctions_Beak : PotionFunctions
{
    [SerializeField] private BeakDebuffType _beakDebuffType;
    private int _beakDamageAdded;
    private int _beakAccuracyReduced;
    private int _BreastResistanceReduced;

    private void Update()
    {
        PriceAndInteractableUpdate();
        PotionDescriptionAndEffectUpdate();
    }

    public override void OnPotionSelection()
    {
        base.OnPotionSelection();
        BeakDamageIncrease();
    }

    private void BeakDamageIncrease()
    {
        switch(_beakDebuffType)
        {
            case BeakDebuffType.DebuffBeakAcc:
                _upgradeManager._chickenStatInUpgrade.AddBeakAttackDamage(_beakDamageAdded);
                _upgradeManager._chickenStatInUpgrade.AddBeakAttackAccuracy(-_beakAccuracyReduced);
                break;
            case BeakDebuffType.DebuffBreastResistance:
                _upgradeManager._chickenStatInUpgrade.AddBeakAttackDamage(_beakDamageAdded);
                _upgradeManager._chickenStatInUpgrade.AddBreastDamageReduction(-_BreastResistanceReduced);
                break;
        }
    }

    private void PotionDescriptionAndEffectUpdate()
    {
        switch (_beakDebuffType)
        {
            case BeakDebuffType.DebuffBeakAcc:
                _beakDamageAdded = 3 * _upgradeManager._displayParameter._beak;
                _beakAccuracyReduced = (2 * (6 - _upgradeManager._displayParameter._beak));
                _descriptionText.text = $"Beak Attack Damage +{_beakDamageAdded}\n" +
                    $"<color=#CB0000>Beak Attack Accuracy -{_beakAccuracyReduced}</color>";
                break;
            case BeakDebuffType.DebuffBreastResistance:
                _beakDamageAdded = 2 * _upgradeManager._displayParameter._beak;
                _BreastResistanceReduced = (3 * (6 - _upgradeManager._displayParameter._beak));
                _descriptionText.text = $"Beak Attack Damage +{_beakDamageAdded}\n" +
                    $"<color=#CB0000>Breast Damage Reduction -{_BreastResistanceReduced}%</color>";
                break;
        }
    }
}
