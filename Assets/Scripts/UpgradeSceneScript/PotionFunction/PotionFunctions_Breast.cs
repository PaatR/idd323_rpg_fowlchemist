using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UtilityType { BreastDamageReduction, BreastHealing, MaxHP}

public class PotionFunctions_Breast : PotionFunctions
{
    [SerializeField] private UtilityType _utilityType;
    private int _breastDamageReductionAdded;
    private int _breastHealingAdded;
    private int _maxHPAdded;

    private void Update()
    {
        PriceAndInteractableUpdate();
        PotionDescriptionAndEffectUpdate();
    }

    public override void OnPotionSelection()
    {
        base.OnPotionSelection();
        UtilityBuff();
    }

    private void UtilityBuff()
    {
        switch(_utilityType)
        {
            case UtilityType.BreastDamageReduction:
                _upgradeManager._chickenStatInUpgrade.AddBreastDamageReduction(_breastDamageReductionAdded);
                break;
            case UtilityType.BreastHealing:
                _upgradeManager._chickenStatInUpgrade.AddBreastHealing(_breastHealingAdded);
                break;
            case UtilityType.MaxHP:
                _upgradeManager._chickenStatInUpgrade.AddMaxHealth(_maxHPAdded);
                break;
        }
    }

    private void PotionDescriptionAndEffectUpdate()
    {
        switch (_utilityType)
        {
            case UtilityType.BreastDamageReduction:
                _breastDamageReductionAdded = 10 * _upgradeManager._displayParameter._breast;
                _descriptionText.text = $"Breast Damage Reduction +{_breastDamageReductionAdded}%";
                break;
            case UtilityType.BreastHealing:
                _breastHealingAdded = 3 * _upgradeManager._displayParameter._breast;
                _descriptionText.text = $"Breast Healing +{_breastHealingAdded}";
                break;
            case UtilityType.MaxHP:
                _maxHPAdded = 8 * _upgradeManager._displayParameter._breast;
                _descriptionText.text = $"Max Health +{_maxHPAdded}";
                break;
        }
    }
}
