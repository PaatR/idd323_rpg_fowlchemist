using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PotionFunctions : MonoBehaviour
{
    //Make this the main potion functions, Inherit each type (breast wings eyes beak)

    [Header("From Potion Children")]
    public Button _buyButton;
    [SerializeField] private TextMeshProUGUI _priceText;
    public TextMeshProUGUI _descriptionText;

    [Header("Value Set")]
    [SerializeField] private int _price;

    [HideInInspector] public UpgradeManager _upgradeManager;
    private GameObject _upgradeScripts;

    private void Start()
    {
        _upgradeScripts = GameObject.Find("UpgradeScripts");
        _upgradeManager = _upgradeScripts.GetComponent<UpgradeManager>();
    }

    public void PriceAndInteractableUpdate()
    {
        _priceText.text = $"{_price} <color=#444444>point(s)</color>";

        if (_price > _upgradeManager._pointSystemInUpgrade._upgradePoint)
        {
            _buyButton.interactable = false;
        }
        //else if (_price <= _upgradeManager._pointSystemInUpgrade._upgradePoint)
        //{
        //    _buyButton.interactable = true;
        //}
    }

    public virtual void OnPotionSelection()
    {
        _upgradeManager._soundEffects.PotionSelectSoundPlay();

        _upgradeManager._pointSystemInUpgrade.PointDecrease(_price);
        _upgradeManager._potionSpawning.IncreasePotionCount(1);
        _upgradeScripts.SendMessage("ClearAndSpawnPotionAfterSelection");
    }
}
