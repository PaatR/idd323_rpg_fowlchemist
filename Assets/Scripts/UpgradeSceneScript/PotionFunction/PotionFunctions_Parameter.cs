using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Paramter { Eyes, Beak, Wings, Breast}

public class PotionFunctions_Parameter : PotionFunctions
{
    [SerializeField] private Paramter _paramter;
    [SerializeField] private int _parameterLevelAdded;

    private void Update()
    {
        PriceAndInteractableUpdate();
        CheckIfParameterLevelIsMax();
        _descriptionText.text = $"{_paramter} Level +{_parameterLevelAdded}";
    }

    public override void OnPotionSelection()
    {
        base.OnPotionSelection();
        ParamterLevelIncrease();
    }

    private void ParamterLevelIncrease()
    {
        switch(_paramter)
        {
            case Paramter.Eyes:
                _upgradeManager._displayParameter.AddEyesLevel(_parameterLevelAdded);
                break;
            case Paramter.Beak:
                _upgradeManager._displayParameter.AddBeakLevel(_parameterLevelAdded);
                break;
            case Paramter.Wings:
                _upgradeManager._displayParameter.AddWingsLevel(_parameterLevelAdded);
                break;
            case Paramter.Breast:
                _upgradeManager._displayParameter.AddBreastLevel(_parameterLevelAdded);
                break;
        }
    }

    private void CheckIfParameterLevelIsMax()
    {
        switch (_paramter)
        {
            case Paramter.Eyes:
                if(_upgradeManager._displayParameter._eyes >= 5)
                {
                    _buyButton.interactable = false;
                }
                break;
            case Paramter.Beak:
                if(_upgradeManager._displayParameter._beak >= 5)
                {
                    _buyButton.interactable = false;
                }
                break;
            case Paramter.Wings:
                if(_upgradeManager._displayParameter._wings >= 5)
                {
                    _buyButton.interactable = false;
                }
                break;
            case Paramter.Breast:
                if(_upgradeManager._displayParameter._breast >= 5)
                {
                    _buyButton.interactable = false;
                }
                break;
        }
    }
}
