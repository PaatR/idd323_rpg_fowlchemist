using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpgradeManager : MonoBehaviour
{
    public DisplayParameter _displayParameter;
    public ChickenStatInUpgrade _chickenStatInUpgrade;
    public PointSystemInUpgrade _pointSystemInUpgrade;
    public PotionSpawning _potionSpawning;
    public SoundEffects _soundEffects;

    private void Start()
    {
        StartUpgradeScene();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            _potionSpawning.ClearAndSpawnPotionAfterSelection();
        }
    }

    private void StartUpgradeScene()
    {
        _displayParameter.ReadChickenParameterSingletonData();
        _chickenStatInUpgrade.ReadChickenStatSingleton();
        _pointSystemInUpgrade.ReadPointSingletonData();

        _potionSpawning.FillPotionChoice();
    }

    public void SavingDataAndGoToCombat()
    {
        //occur when press go to combat button

        //save all singletons
        _displayParameter.SaveChickenParameter();
        _chickenStatInUpgrade.SaveChickenStat();

        //clear upgrade point to let the player gather again
        PointSingleton.instance.ClearUpgradePoint();

        //change to combat scene
        SceneManager.LoadScene("Combat");
    }
}
