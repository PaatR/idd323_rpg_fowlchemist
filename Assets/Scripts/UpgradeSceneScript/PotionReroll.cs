using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PotionReroll : MonoBehaviour
{
    [SerializeField] private UpgradeManager _upgradeManager;
    [SerializeField] private Button _rerollButton;
    [SerializeField] private TextMeshProUGUI _rerollPriceText;
    private int _rerollPrice;

    private void Start()
    {
        _rerollPrice = 0;
    }

    private void Update()
    {
        InteractableCheck();
        RerollPriceTextUpdate();
    }

    public void OnRerollClick()
    {
        _upgradeManager._potionSpawning.ClearAndSpawnPotionAfterSelection();
        _upgradeManager._pointSystemInUpgrade.PointDecrease(_rerollPrice);
        _rerollPrice += 1;
    }

    private void InteractableCheck()
    {
        if (_rerollPrice > _upgradeManager._pointSystemInUpgrade._upgradePoint)
        {
            _rerollButton.interactable = false;
        }
        else if (_rerollPrice < _upgradeManager._pointSystemInUpgrade._upgradePoint)
        {
            _rerollButton.interactable = true;
        }
    }

    private void RerollPriceTextUpdate()
    {
        _rerollPriceText.text = $"{_rerollPrice} points";
    }
}
