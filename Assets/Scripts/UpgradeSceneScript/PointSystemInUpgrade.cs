using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PointSystemInUpgrade : MonoBehaviour
{
    [Header("UPGRADE POINT")]
    public int _upgradePoint;
    [SerializeField] private TextMeshProUGUI UpgradePointText;

    private void Update()
    {
        UpgradePointText.text = $"<color=#A8A8A8>Point: </color>" + _upgradePoint.ToString();

        if (Input.GetKeyDown(KeyCode.T))
        {
            PointIncrease(5);
        }
    }

    public void SetUpgradePoint(int _point)
    {
        _upgradePoint = _point;
    }

    public void PointIncrease(int _increase)
    {
        _upgradePoint += _increase;
    }

    public void PointDecrease(int _decrease)
    {
        _upgradePoint -= _decrease;
    }    

    public void ReadPointSingletonData()
    {
        if (PointSingleton.instance.hasUpgradePointData == true)
        {
            SetUpgradePoint(PointSingleton.instance.UpgradePoint);
        }
        else if (PointSingleton.instance.hasUpgradePointData == false)
        {
            SetUpgradePoint(0);
        }
    }

    public void SavePoint()
    {
        PointSingleton.instance.SaveUpgradePoint(_upgradePoint);
    }
}
