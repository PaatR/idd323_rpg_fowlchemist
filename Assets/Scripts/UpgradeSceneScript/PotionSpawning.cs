using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PotionSpawning : MonoBehaviour
{
    [SerializeField] private UpgradeManager _upgradeManager;
    [SerializeField] private GameObject _potionGroup;
    [SerializeField] private GameObject[] _potion;
    [SerializeField] private TextMeshProUGUI _potionCountText;
    private int _potionCount;
    private bool _startClearAndSpawn = false;

    private void Start()
    {
        _potionCount = 0;
    }

    private void Update()
    {
        _potionCountText.text = $"Potions Selected: <color=#E7A02B>{_potionCount}</color>";
    }

    public void IncreasePotionCount(int increase)
    {
        _potionCount += increase;
    }
    public void ClearAndSpawnPotionAfterSelection()
    {
        StartCoroutine(ClearAndSpawnDelay());
    }
    public void FillPotionChoice()
    {
        for(int i = 0; i < 3; i++)
        {
            SpawnPotion();
        }
    }
    private void ClearPotion()
    {
        foreach(Transform potion in _potionGroup.transform)
        {
            Destroy(potion.gameObject);
        }
    }
    private void SpawnPotion()
    {
        GameObject _randomPotion = _potion[Random.Range(0, _potion.Length)];
        Instantiate(_randomPotion, _potionGroup.transform);
    }
    IEnumerator ClearAndSpawnDelay()
    {
        if(_startClearAndSpawn == false)
        {
            _startClearAndSpawn = true;
            ClearPotion();
            yield return new WaitForSeconds(0.25f);
            FillPotionChoice();
            _startClearAndSpawn = false;
        }
    }
}
