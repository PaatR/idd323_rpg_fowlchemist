using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenStatInUpgrade : MonoBehaviour
{
    public int _maxHealth { get; private set; }
    public int _beakAttackDamage { get; private set; }
    public int _beakAttackAccuracy { get; private set; }
    public int _wingsAttackDamage { get; private set; }
    public int _wingsAttackAccuracy { get; private set; }
    public int _breastDamageReduction { get; private set; }
    public int _breastHealing { get; private set; }

    public void ReadChickenStatSingleton()
    {
        if (ChickenStatSingleton.instance.hasStatData == true)
        {
            _maxHealth = ChickenStatSingleton.instance.maxHealth;
            _beakAttackDamage = ChickenStatSingleton.instance.beakAttackDamage;
            _beakAttackAccuracy = ChickenStatSingleton.instance.beakAttackAccuracy;
            _wingsAttackDamage = ChickenStatSingleton.instance.wingsAttackDamage;
            _wingsAttackAccuracy = ChickenStatSingleton.instance.wingsAttackAccuracy;
            _breastDamageReduction = ChickenStatSingleton.instance.breastDamageReduction;
            _breastHealing = ChickenStatSingleton.instance.breastHealing;
        }
        else if (ChickenStatSingleton.instance.hasStatData == false)
        {
            _maxHealth = 100;
            _beakAttackDamage = 10;
            _beakAttackAccuracy = 90;
            _wingsAttackDamage = 5;
            _wingsAttackAccuracy = 80;
            _breastDamageReduction = 20;
            _breastHealing = 5;
        }
    }

    public void SaveChickenStat()
    {
        ChickenStatSingleton.instance.SaveChickenStat(_maxHealth, _beakAttackDamage, _beakAttackAccuracy,
            _wingsAttackDamage, _wingsAttackAccuracy, _breastDamageReduction, _breastHealing);
    }

    public void AddMaxHealth(int increase)
    {
        _maxHealth += increase;
    }
    public void AddBeakAttackDamage(int increase)
    {
        _beakAttackDamage += increase;
    }
    public void AddBeakAttackAccuracy(int increase)
    {
        _beakAttackAccuracy += increase;
    }
    public void AddWingsAttackDamage(int increase)
    {
        _wingsAttackDamage += increase;
    }
    public void AddWingsAttackAccuracy(int increase)
    {
        _wingsAttackAccuracy += increase;
    }
    public void AddBreastDamageReduction(int increase)
    {
        _breastDamageReduction += increase;
    }
    public void AddBreastHealing(int increase)
    {
        _breastHealing += increase;
    }

    public void SetValueToStandardLimit()
    {
        if(_beakAttackDamage < 0) { _beakAttackDamage = 0; }
        if(_beakAttackAccuracy < 0) { _beakAttackAccuracy = 0; }
        if(_beakAttackAccuracy > 100) { _beakAttackAccuracy = 100; }
        if(_wingsAttackDamage < 0) { _wingsAttackDamage = 0; }
        if (_wingsAttackAccuracy < 0) { _wingsAttackAccuracy = 0; }
        if(_wingsAttackAccuracy > 100) { _wingsAttackAccuracy = 100; }
        if(_breastDamageReduction < 0) { _breastDamageReduction = 0; }
        if(_breastDamageReduction > 100) { _breastDamageReduction = 100; }
        if (_breastHealing < 0) { _breastHealing = 0; }
    }
}
