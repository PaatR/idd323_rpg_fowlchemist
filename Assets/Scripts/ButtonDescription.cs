using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ButtonDescription : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private int _buttonID;
    [Space]
    [SerializeField] private CombatManager _combatManager;
    [SerializeField] private TextMeshProUGUI _actionNameText;
    [SerializeField] private TextMeshProUGUI _actionDescriptionText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        ButtonIdCheck();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _actionNameText.text = "";
        _actionDescriptionText.text = "";
    }

    private void ButtonIdCheck()
    {
        switch(_buttonID)
        {
            case 0:
                _actionNameText.text = $"<color=#B37D7D>Beak Attack (Single)</color>";

                _actionDescriptionText.text = $"<color=#C9C9C9>Damage:</color> <u>{_combatManager._chickenStatInCombat._beakAttackDamage}</u>" +
                    $"\n<color=#C9C9C9>Accuracy:</color> <u>{_combatManager._chickenStatInCombat._beakAttackAccuracy}%</u>";
                break;

            case 1:
                _actionNameText.text = $"<color=#B37D7D>Wings Attack (All)</color>";

                _actionDescriptionText.text = $"<color=#C9C9C9>Damage:</color> <u>{_combatManager._chickenStatInCombat._wingsAttackDamage}</u>" +
                    $"\n<color=#C9C9C9>Accuracy:</color> <u>{_combatManager._chickenStatInCombat._wingsAttackAccuracy}%</u>";

                break;

            case 2:
                _actionNameText.text = $"<color=#7AAE90>Guard</color>";

                _actionDescriptionText.text = $"<color=#C9C9C9>Healing:</color> <u>{_combatManager._chickenStatInCombat._breastHealing}</u>" +
                    $"\n<color=#C9C9C9>Damage Reduction:</color> <u>{_combatManager._chickenStatInCombat._breastDamageReduction}%</u>";

                break;
        }
    }
}
